package ivan.essence.softtecotest.rest;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RESTClient {

    private static final String URL = "http://jsonplaceholder.typicode.com/";

    private OkHttpClient client;
    private Retrofit retrofit;

    private Retrofit getClient() {
        return retrofit != null ? retrofit :new Retrofit.Builder()
                .baseUrl(URL)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private OkHttpClient getOkHttpClient() {
       return client != null ? client : new OkHttpClient.Builder()
                .addNetworkInterceptor(new HttpLoggingInterceptor().
                        setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS).build();

    }

    public static RESTInterface getApiInterface() {
        RESTClient restClient = new RESTClient();
        return restClient.getClient().create(RESTInterface.class);
    }

}
