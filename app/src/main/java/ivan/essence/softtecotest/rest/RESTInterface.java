package ivan.essence.softtecotest.rest;

import java.util.List;

import ivan.essence.softtecotest.pojos.User;
import ivan.essence.softtecotest.pojos.UserPost;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RESTInterface {

    @GET("posts")
    Call<List<UserPost>> getPosts();

    @GET("users/{id}")
    Call<User> getUserProfile(@Path("id") long userId);
}
