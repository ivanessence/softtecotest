package ivan.essence.softtecotest.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Calendar;

import ivan.essence.softtecotest.activities.main.AnimationListener;

/**
 * Created by ivan on 02.11.2018.
 */
public class AndroidUtils {

    private static final String FOLDER_NAME = "LogCat";
    private static final String FILE_NAME = "log-";

    public static RotateAnimation animateImage(final AnimationListener animationListener) {
        RotateAnimation anim = new RotateAnimation(0f, 350f, 35f, 35f);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                animationListener.showItem(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.ABSOLUTE);
        anim.setDuration(2200);
        anim.hasEnded();
        return anim;
    }

    public static void itemAnimation(View view) {
        ScaleAnimation animation  = new ScaleAnimation(0F,1F,0F,1F);
        animation.setDuration(500);
        view.startAnimation(animation);
    }

    public static void saveLogcatFile(final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                OutputStreamWriter outputStreamWriter = null;
                try {
                    Process process = Runtime.getRuntime().exec("logcat -d");
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    StringBuilder loggedText = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        loggedText.append(line);
                        loggedText.append("\n");
                    }
                    File sdCard = context.getExternalFilesDir(null);
                    File dir = new File(sdCard + File.separator + FOLDER_NAME);
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    File file = new File(dir, FILE_NAME + Calendar.getInstance().getTime() + ".txt");
                    outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
                    outputStreamWriter.write(loggedText.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStreamWriter != null) {
                            outputStreamWriter.flush();
                            outputStreamWriter.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
