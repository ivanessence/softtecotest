package ivan.essence.softtecotest.activities.main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;

import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ivan.essence.softtecotest.R;
import ivan.essence.softtecotest.activities.BaseActivity;
import ivan.essence.softtecotest.adapters.PostsAdapter;
import ivan.essence.softtecotest.pojos.UserPost;
import ivan.essence.softtecotest.rest.RESTClient;
import ivan.essence.softtecotest.utils.AndroidUtils;
import ivan.essence.softtecotest.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.tinkoff.scrollingpagerindicator.ScrollingPagerIndicator;

public class MainActivity extends BaseActivity implements AnimationListener {

    private MenuItem menuItemSave;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imgAnon)
    ImageView imgAnon;
    @BindView(R.id.postsRecycler)
    RecyclerView postsRecycler;
    @BindView(R.id.indicator)
    ScrollingPagerIndicator indicator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        makeRequest();
        imgAnon.setAnimation(AndroidUtils.animateImage(this));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == Constants.PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                boolean externalStorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (externalStorage) {
                    showToast(R.string.permissionWasGranted, true);
                    AndroidUtils.saveLogcatFile(this);
                } else {
                    showToast(R.string.permissionWasDenied, true);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        menuItemSave = menu.findItem(R.id.item_save);
        showItem(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_save:
                permissionMethod();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void initialization() {
        ButterKnife.bind(this);
        postsRecycler.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(MainActivity.this, 2, GridLayoutManager.HORIZONTAL, false);
        postsRecycler.setLayoutManager(gridLayoutManager);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(postsRecycler);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public void showItem(boolean visibility) {
        menuItemSave.setVisible(visibility);
    }

    @Override
    protected void makeRequest() {
        super.makeRequest();
        Call<List<UserPost>> call = RESTClient.getApiInterface().getPosts();
        call.enqueue(new Callback<List<UserPost>>() {
            @Override
            public void onResponse(Call<List<UserPost>> call, Response<List<UserPost>> response) {
                postsRecycler.setAdapter(new PostsAdapter(response.body(), MainActivity.this));
                indicator.attachToRecyclerView(postsRecycler);
                dismissAlertDialog();
            }

            @Override
            public void onFailure(Call<List<UserPost>> call, Throwable t) {
                dismissAlertDialog();
            }
        });
    }

    private void permissionMethod() {
        if (Build.VERSION.SDK_INT >= 21) {
            if (!checkPermission()) {
                requestPermission();
            } else {
                AndroidUtils.saveLogcatFile(this);
            }
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.PERMISSION_REQUEST_CODE);
    }

    private boolean checkPermission() {
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return storage == PackageManager.PERMISSION_GRANTED;
    }
}
