package ivan.essence.softtecotest.activities.postDetails;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ivan.essence.softtecotest.R;
import ivan.essence.softtecotest.activities.BaseActivity;
import ivan.essence.softtecotest.activities.map.MapActivity;
import ivan.essence.softtecotest.database.AppDatabase;
import ivan.essence.softtecotest.pojos.Geo;
import ivan.essence.softtecotest.pojos.User;
import ivan.essence.softtecotest.rest.RESTClient;
import ivan.essence.softtecotest.utils.AndroidUtils;
import ivan.essence.softtecotest.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostDetailsActivity extends BaseActivity {

    private long postID;
    private User user;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnSaveToDatabase)
    FloatingActionButton btnSaveToDatabase;
    @BindView(R.id.postIdentificator)
    TextView postIdentificator;
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.userNickName)
    TextView userNickName;
    @BindView(R.id.userEmail)
    TextView userEmail;
    @BindView(R.id.userWebsite)
    TextView userWebsite;
    @BindView(R.id.userPhone)
    TextView userPhone;
    @BindView(R.id.userCity)
    TextView userCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        postID = getIntent().getLongExtra("postId", 0);
        makeRequest();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == Constants.PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                boolean phoneCall = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (phoneCall) {
                    showToast(R.string.permissionWasGranted, true);
                    createCall();
                } else {
                    showToast(R.string.permissionWasDenied, true);
                }
            }
        }
    }

    @Override
    protected void initialization() {
        ButterKnife.bind(this);
        toolbar.setTitle(getString(R.string.app_name));
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_post_details;
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @OnClick({R.id.btnSaveToDatabase, R.id.userCity, R.id.userPhone})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.btnSaveToDatabase:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            AppDatabase.getInstance(PostDetailsActivity.this).databaseInteraction().insert(user);
                            showToast(R.string.save_to_database, false);
                        } catch (Exception e) {
                            showToast(R.string.error_save_to_database, false);
                        }
                    }
                }).start();
                break;
            case R.id.userCity:
                if (user != null) { //also we can put Parcelable object or find user in database in MapActivity, if user saved it
                    Geo geo = user.getAddress().getGeo();
                    double latitude = geo.getLat();
                    double longitude = geo.getLng();
                    Intent intent = new Intent(this, MapActivity.class);
                    intent.putExtra("latitude", latitude);
                    intent.putExtra("longitude", longitude);
                    intent.putExtra("city", user.getAddress().getCity());
                    startActivity(intent);
                } else {
                    showToast(R.string.address_not_found, true);
                }
                break;
            case R.id.userPhone:
                if (user != null) {
                    permissionMethod();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void makeRequest() {
        super.makeRequest();
        Call<User> call = RESTClient.getApiInterface().getUserProfile(postID);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                user = response.body();
                if (user != null) setupUserData(user);
                dismissAlertDialog();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                dismissAlertDialog();
            }
        });
    }

    private void setupUserData(User user) {
        getSupportActionBar().setTitle(getString(R.string.contact_id, user.getId()));
        postIdentificator.setText(String.valueOf(postID));
        userName.setText(user.getName());
        userNickName.setText(user.getUsername());
        userEmail.setText(user.getEmail());
        userWebsite.setText(user.getWebsite());
        userPhone.setText(user.getPhone());
        userCity.setPaintFlags(userCity.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        userCity.setText(user.getAddress().getCity());
    }

    private void createCall() {
        String number = ("tel:" + user.getPhone());
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(number));
        startActivity(intent);
    }

    private void permissionMethod() {
        if (Build.VERSION.SDK_INT >= 21) {
            if (!checkPermission()) {
                requestPermission();
            } else {
                createCall();
            }
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, Constants.PERMISSION_REQUEST_CODE);
    }

    private boolean checkPermission() {
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        return storage == PackageManager.PERMISSION_GRANTED;
    }
}

