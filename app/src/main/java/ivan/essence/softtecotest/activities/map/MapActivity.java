package ivan.essence.softtecotest.activities.map;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import ivan.essence.softtecotest.R;
import ivan.essence.softtecotest.activities.BaseActivity;

public class MapActivity extends BaseActivity implements OnMapReadyCallback {

    private double latitude;
    private double longitude;
    private String city;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        latitude = getIntent().getDoubleExtra("latitude", 0);
        longitude = getIntent().getDoubleExtra("longitude", 0);
        city = getIntent().getStringExtra("city");
    }

    @Override
    protected void initialization() {
        ButterKnife.bind(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_map;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        customizeMap(googleMap);
        LatLng sydney = new LatLng(latitude, longitude);
        googleMap.addMarker(new MarkerOptions().position(sydney).title(city)).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.home));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    private void customizeMap(GoogleMap googleMap) {
        try {
            googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }
}
