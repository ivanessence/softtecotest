package ivan.essence.softtecotest.activities.main;

public interface AnimationListener {
    void showItem(boolean visibility);
}
