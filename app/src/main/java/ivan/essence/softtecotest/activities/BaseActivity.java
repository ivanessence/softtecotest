package ivan.essence.softtecotest.activities;

import android.app.AlertDialog;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import dmax.dialog.SpotsDialog;
import ivan.essence.softtecotest.R;

public abstract class BaseActivity extends AppCompatActivity {

    protected AlertDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        initialization();
        setSupportActionBar(getToolbar());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void showToast(@StringRes final int message, boolean isUIThread) {
        if (isUIThread) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        } else {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(BaseActivity.this, message, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    protected abstract void initialization();

    protected abstract int getContentView();

    protected abstract Toolbar getToolbar();

    protected void makeRequest() {
        createProgressDialog();
    }

    protected void dismissAlertDialog() {
        progressDialog.dismiss();
    }

    private void createProgressDialog() {
        progressDialog = new SpotsDialog.Builder()
                .setContext(this).setTheme(R.style.ProgressDialogStyle).build();
        progressDialog.show();
    }

    protected void setupActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
