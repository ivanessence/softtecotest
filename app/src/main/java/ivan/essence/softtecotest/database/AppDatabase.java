package ivan.essence.softtecotest.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import ivan.essence.softtecotest.pojos.User;

@Database(entities = {User.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase appDatabaseInstance;
    public abstract DatabaseInteraction databaseInteraction();

    public static AppDatabase getInstance(Context context) {
        if (appDatabaseInstance == null) {
            appDatabaseInstance =
                    Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "appDatabase")
                            .build();
        }
        return appDatabaseInstance;
    }

    public static void destroyInstance() {
        appDatabaseInstance = null;
    }

}
