package ivan.essence.softtecotest.database;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Update;

import ivan.essence.softtecotest.pojos.User;

@Dao
public interface DatabaseInteraction {

    @Insert
    void insert(User object);

    @Update
    void update(User object);

    @Delete
    void delete(User object);
}
