package ivan.essence.softtecotest.pojos;
import android.arch.persistence.room.ColumnInfo;

public class Company {

    @ColumnInfo(name = "catch_phrase")
    private String catchPhrase;
    @ColumnInfo(name = "company_name")
    private String name;
    @ColumnInfo(name = "bs")
    private String bs;

    public String getCatchPhrase() {
        return catchPhrase;
    }

    public void setCatchPhrase(String catchPhrase) {
        this.catchPhrase = catchPhrase;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBs() {
        return bs;
    }

    public void setBs(String bs) {
        this.bs = bs;
    }

    @Override
    public String toString() {
        return "ClassPojo [catchPhrase = " + catchPhrase + ", name = " + name + ", bs = " + bs + "]";
    }
}
