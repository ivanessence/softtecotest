package ivan.essence.softtecotest.pojos;

import android.arch.persistence.room.ColumnInfo;

public class Geo {

    @ColumnInfo(name = "lng")
    private double lng;
    @ColumnInfo(name = "lat")
    private double lat;

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @Override
    public String toString() {
        return "ClassPojo [lng = " + lng + ", lat = " + lat + "]";
    }
}
