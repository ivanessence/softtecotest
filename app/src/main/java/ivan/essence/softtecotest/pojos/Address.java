package ivan.essence.softtecotest.pojos;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;

public class Address {
    @Embedded
    private Geo geo;
    @ColumnInfo(name = "zip_code")
    private String zipcode;
    @ColumnInfo(name = "street")
    private String street;
    @ColumnInfo(name = "suite")
    private String suite;
    @ColumnInfo(name = "city")
    private String city;

    public Geo getGeo() {
        return geo;
    }

    public void setGeo(Geo geo) {
        this.geo = geo;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "ClassPojo [geo = " + geo + ", zipcode = " + zipcode + ", street = " + street + ", suite = " + suite + ", city = " + city + "]";
    }
}
