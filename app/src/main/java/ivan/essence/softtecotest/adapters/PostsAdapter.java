package ivan.essence.softtecotest.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ivan.essence.softtecotest.R;
import ivan.essence.softtecotest.activities.postDetails.PostDetailsActivity;
import ivan.essence.softtecotest.pojos.UserPost;
import ivan.essence.softtecotest.utils.AndroidUtils;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostsAdapterViewHolder> {

    private List<UserPost> usersPostsList;
    private Context context;
    private int lastPosition = -1;

    public PostsAdapter(List<UserPost> usersPostList, Context context) {
        this.context = context;
        this.usersPostsList = usersPostList;
    }

    public class PostsAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.postIdentificator)
        TextView postId;
        @BindView(R.id.postTitle)
        TextView postTitle;

        private PostsAdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick()
        void click(View view) {
           Intent intent = new Intent(context, PostDetailsActivity.class);
           intent.putExtra("postId", usersPostsList.get(getAdapterPosition()).getUserId());
           context.startActivity(intent);
        }
    }

    @NonNull
    @Override
    public PostsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int width = parent.getWidth() / 3;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_view, parent, false);
        view.setLayoutParams(new FrameLayout.LayoutParams(width, FrameLayout.LayoutParams.WRAP_CONTENT));
        return new PostsAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostsAdapterViewHolder holder, int position) {
        UserPost userPost = usersPostsList.get(position);
        holder.postId.setText(String.valueOf(userPost.getId()));
        holder.postTitle.setText(userPost.getTitle());
        animateItem(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return usersPostsList.size();
    }

    private void animateItem(View view, int position){
        if(position > lastPosition){
           AndroidUtils.itemAnimation(view);
            lastPosition = position;
        }
    }
}
